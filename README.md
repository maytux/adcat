#High level view of requests processing
User -> Internet Gateway -> Load Balancer -> Application Replicas
Subnets: app-subnet, management-subnet (jenkins) -> IG -> Outside traffic

#Web Application Deployment [Blue-green: LB1-Service1,Service2, Canary Deployment: LB1-Service1, LB2-Service2, weighted routing by service route53 to both LB's]
Bitbucket/Gitlab/Github (webhook triggering upon condition of code merge/pull request/else) -> Jenkins start Pipeline, code written in Jenkinsfile present in source code or jobs on server (steps: pull the code, setup environment variables according to different environments dev/QA/Prod, Create docker image from Dockerfile, push docker image to AWS ECR repository and deploy app from that image on AWS ECS cluster)

#DR Strategy
Data backup policy should be maintained with proper frequency of incremental and full backups of application database, Logs backup should be maintained for atleast 1 year, infra code should be written in manner to get buld up in any region.

#Code
#!/bin/sh
git clone https://maytux@bitbucket.org/maytux/adcat.git
#First create the VPC,ECS cluster and lanuch an ECS instance by terraform
#Provide aws credentials in adcat/terraform/vpc/variables.tf file
cd adcat/vpc
terraform init
terraform plan
terraform apply

#Register task definition in AWS ECS
aws ecs register-task-definition --cli-input-json file://task-definition/blog-task.json
aws ecs register-task-definition --cli-input-json file://task-definition/jenkins-task.json

#Create Application Load Balancer
aws elbv2 create-load-balancer --name my-application-load-balancer --subnets subnet-id1 subnet-id2 --security-groups sg-id

#Create Target group
aws elbv2 create-target-group --name my-targets --protocol HTTP --port 80 --vpc-id vpc-id

#Create listner in LB 
aws elbv2 create-listener --load-balancer-arn loadbalancer-arn --protocol HTTP --port 80 --default-actions Type=forward,TargetGroupArn=targetgroup-arn

#Path based routing, add different rules in ALB 
aws elbv2 create-rule --listener-arn listener-arn --priority 10 \
--conditions Field=path-pattern,Values='/blog' \
--actions Type=forward,TargetGroupArn=targetgroup-arn

#Create service for running number of replica's of tasks and service discovery and load balancer settings setup EC2 type
aws ecs create-service --cluster my-ecs-cluster --service-name WEB-EC2-service --task-definition blog:1 --desired-count 3 --launch-type "EC2" --load-balancers "targetGroupArn=arn:aws:elasticloadbalancing:us-east-1:xxxxxx:targetgroup/blog-TG/id,containerName=blog-task,containerPort=8080"

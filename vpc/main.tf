provider "aws" {
  region = "${var.region}"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}

# This is where we store the state
terraform {
  backend "s3" {
    bucket = "x.y.z"
    key    = "vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

# We define the VPC here
resource "aws_vpc" "myvpc" {
  cidr_block = "10.0.0.0/16"
  tags{
    Name = "My VPC in us-east-1"
  }
}

# We define a private subnet in us-east-1a
resource "aws_subnet" "private-1a" {
  vpc_id = "${aws_vpc.myvpc.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags{
    Name = "Private 1a Subnet"
  }
}

# Another private subnet in us-east-1d
resource "aws_subnet" "private-1d" {
  vpc_id = "${aws_vpc.myvpc.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1d"
  tags{
    Name = "Private 1d Subnet"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "myigw" {
  vpc_id = "${aws_vpc.myvpc.id}"
  tags {
    Name = "MyVPC Inernet Gateway"
  }
}

# Create a private route table
resource "aws_route_table" "private_route_table" {
  vpc_id = "${aws_vpc.myvpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.myigw.id}"
  }
  tags{
    Name = "Private Route Table"
  }
}

# Associate the private route table with the 2 private subnets
resource "aws_route_table_association" "private_rt_association_1a" {
  subnet_id      = "${aws_subnet.private-1a.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_route_table_association" "private_rt_association_1d" {
  subnet_id      = "${aws_subnet.private-1d.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_iam_instance_profile" "ecs-instance" {
  name = "ecs-instance"
  role = "${aws_iam_role.ecs-instance.name}"
}

resource "aws_iam_role" "ecs-instance" {
  name               = "ecs-instance"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_ecs_instance_policy")}"
}


resource "aws_iam_role_policy" "ecs-instance" {
  name   = "ecs-instance"
  role   = "${aws_iam_role.ecs-instance.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_ecs_instance_policy")}"
}


resource "aws_key_pair" "ecs-instance-key-pair" {
  key_name   = "ecs-instance-key-pair"
  public_key = "${file("${path.module}/data/aws_key_pair_ecs_instance_public_key")}"
}

resource "aws_security_group" "app" {
  name        = "app"
  vpc_id      = "${aws_vpc.myvpc.id}"
  description = "Security group for app"

  tags = {
    Name              = "app"
  }
}

resource "aws_security_group" "jenkins" {
  name        = "jenkins"
  vpc_id      = "${aws_vpc.myvpc.id}"
  description = "Security group for jenkins"

  tags = {
    Name              = "jenkins"
  }
}

resource "aws_security_group" "lb" {
  name        = "lb"
  vpc_id      = "${aws_vpc.myvpc.id}"
  description = "Security group for lb"

  tags = {
    Name              = "lb"
  }
}

resource "aws_security_group_rule" "outside-to-lb" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.lb.id}"
  from_port                = 80 
  to_port                  = 80
  protocol                 = "tcp"
  cidr_blocks       	   = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "lb-app" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.app.id}"
  source_security_group_id = "${aws_security_group.lb.id}"
  from_port                = 80
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "bitbucket-jenkins" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.jenkins.id}"
  from_port                = 1
  to_port                  = 8080
  protocol                 = "tcp"
  cidr_blocks       	   = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "jenkins-slave-jenkins-master" {
  type              = "ingress"
  security_group_id = "${aws_security_group.jenkins.id}"
  from_port         = 1
  to_port           = 50000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "lb-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.lb.id}"
  source_security_group_id = "${aws_security_group.app.id}"
  from_port         = 1
  to_port           = 65535
  protocol          = "tcp"
}

resource "aws_security_group_rule" "app-outside" {
  type              = "egress"
  security_group_id = "${aws_security_group.app.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "jenkins-outside" {
  type                     = "egress"
  security_group_id        = "${aws_security_group.jenkins.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "tcp"
  cidr_blocks       	   = ["0.0.0.0/0"]
}

output "vpc_id" {
  value = "${aws_vpc.myvpc.id}"
}

output "private-1a_subnet_id" {
  value = "${aws_subnet.private-1a.id}"
}

output "private-1d_subnet_id" {
  value = "${aws_subnet.private-1d.id}"
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "my-ecs-cluster"
}

# Next, we will create a launch config for the ECS cluster
resource "aws_launch_configuration" "ecs_launch_config" {
  name_prefix   = "my-ecs-launch-config"
  image_id      = "${var.ecs_ami_id}"
  instance_type = "t2.micro"
  key_name                = "${aws_key_pair.ecs-instance-key-pair.id}"
  security_groups         = ["${aws_security_group.app.id}"]
  iam_instance_profile    = "${aws_iam_instance_profile.ecs-instance.id}"
  associate_public_ip_address = true
  lifecycle {
    create_before_destroy = true
  }
  user_data    = <<EOF
                  #!/bin/bash
                  echo ECS_CLUSTER=${aws_ecs_cluster.ecs_cluster.name} >> /etc/ecs/ecs.config
                  EOF
}

# Now for the auto scaling group
resource "aws_autoscaling_group" "ecs_asg" {
  name                 = "my-ecs-asg"
  vpc_zone_identifier = ["${aws_subnet.private-1d.id}","${aws_subnet.private-1a.id}"]
  health_check_type    = "EC2"
  launch_configuration = "${aws_launch_configuration.ecs_launch_config.name}"
  min_size             = 1
  max_size             = 1
  desired_capacity     = 1
  lifecycle {
    create_before_destroy = true
  }
  tag {
   key                 = "type"
   value               = "ecs"
   propagate_at_launch = true
 }
}


variable "region" {
  description = "The AWS Region"
  default = "us-east-1"
}
variable "access_key" {
  description = "The AWS ACCESS KEY"
  default = ""
}
variable "secret_key" {
  description = "The AWS SECRET KEY"
  default = ""
}
variable "ecs_ami_id" {
  description = "The ECS AMI ID"
  default = "ami-00525183943e7dd3a"
}
